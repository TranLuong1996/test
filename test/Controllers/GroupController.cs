﻿using System;
using System.Threading.Tasks;
using test.Models;
using test.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
namespace test.Controllers
{
    [Route("api/group")]
    public class GroupController: Controller
    {
        private readonly IGroupRepository _group;
        private readonly IProductRepository _product;

        public GroupController(IGroupRepository iGroupRepository, IProductRepository iProductRepository)
        {
            _group = iGroupRepository;
            _product = iProductRepository;
        }

        [HttpGet("list")]
        public async Task<IActionResult> getGroups()
        {
            var groups = await _group.getAll();
            return Ok(groups);
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> getGroupById(int id)
        {
            var group = await _group.getGroupById(id);
            return Ok(group);
        }

        [Authorize(Roles = "1")]
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create(int id, string name,int cate_id){


            var group = new Group
            {
                name = name,
                cateid = cate_id
            };
            await _group.Create(group);
            return Ok(new
            {
                msg = "Success!",
                group = group
            });
        }

        [Authorize(Roles = "1")]
        [HttpPut("{id}/cate/{fk}")]
        public async Task<IActionResult> Update(int id, int fk, string name){
            var group = await _group.Get(id);
            if (group == null) return NotFound();
            group.name = group.name.Equals(name) ? group.name : name;
            group.cateid = group.cateid != 0 ? fk : group.cateid;
            await _group.Update(id, group);
            return Ok(new
            {
                msg = "Updated!",
                group = group
            });
           
        }

        [Authorize(Roles = "1")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _group.Delete(id);
            return Ok(new
            {
                msg = "Deleted!"
            });
        }
    }
}
