﻿using System;
using System.Threading.Tasks;
using test.Models;
using test.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace test.Controllers
{
    [Route("api/user")]
    public class UserController : Controller
    {

        private readonly IUserRepository _user;
      

        public UserController(IUserRepository iUserRepository)
        {
            _user = iUserRepository;
        }

        [Authorize(Roles = "1")]
        [HttpGet]
        [Route("list")]
        public async Task<IActionResult> List()
        {
            var users = await _user.getAll();
            return Ok(new { msg = "Success!", users = users });
        }

        [Authorize(Roles = "1")]
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create(User u)
        {
            await _user.Create(u);
            return Ok(new
            {
                msg = "Success!",
                user = u
            });
        }

        [Authorize(Roles = "1")]
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _user.Delete(id);
            return Ok(new { msg = "Success!" });
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Update(int id, string name, string address, string phoneNumber, string password)
        {
            var user = await _user.Get(id);
            if (user == null) return NotFound();
            user.name = user.name.Equals(name) ? user.name : name;
            user.address = user.address.Equals(address) ? user.address : address;
            user.phone_number = user.address.Equals(phoneNumber) ? user.phone_number : phoneNumber;
            user.password = user.password.Equals(password) ? user.password : password;
            await _user.Update(id, user);

            return Ok(new { user = user });
        }

       
    }
}
