﻿using System;
using System.Threading.Tasks;
using test.Models;
using test.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
namespace test.Controllers
{
    [Route("api/cart")]
    public class CartController : Controller
    {
        private readonly ICartRepository _cart;
        private readonly IProductRepository _product;
        public CartController(ICartRepository iCartRepository, IProductRepository iProductRepository)
        {
            _cart = iCartRepository;
            _product = iProductRepository;
        }

        [Authorize(Roles = "1")]
        [HttpGet("list")]
        public async Task<ActionResult> getCarts(){
            var carts = await _cart.getAll();
            return Ok(carts);
        }


        [HttpPost("create")]
        public async Task<ActionResult> Create(string id,
                                               int user_id, int product_id)
        {

            var product = await _product.getProductById(product_id);

            var item = new Cart
            {
                userid = user_id,
                productid = product_id
            };
            await _cart.Create(item);
            return Ok(new
            {
                msg = "Success!",
                cart = item
            });

        }

        [HttpGet("{user_id}")]
        public async Task<ActionResult> getCartByUser(int user_id){
            var carts = await _cart.getCartByUser(user_id);
            return Ok((carts));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _cart.Get(id);
            if (item == null) return NotFound();
            await _cart.Delete(id);
            return Ok(new
            {
                msg = "Deleted!"
            });
        }
    }
}
