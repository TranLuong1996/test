﻿using System;
using System.Threading.Tasks;
using test.Models;
using test.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace test.Controllers
{
    [Authorize(Roles = "1")]
    [Route("api/category")]
    public class CategoryController : Controller
    {
        private readonly ICategoryRepository _category;
        
        public CategoryController(ICategoryRepository iCategoryRepository)
        {
            _category = iCategoryRepository;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> getCateById(int id)
        {
            var cate = await _category.getCateById(id);
            return Ok(cate);
        }

        [HttpGet("list")]
        public async Task<IActionResult> List()
        {
            var cates = await _category.getAll();
            return Ok(cates);
        }

        [Authorize(Roles = "1")]
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create(string name)
        {
            var c = new Category { name = name };
            await _category.Create(c);
            return Ok(new{ msg = "Added!",id = c.id,name = c.name});
        }

        [Authorize(Roles = "1")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, string name)
        {
            if (name == null)
            {
                return BadRequest("Name is requried!");
            }
            var cate = await _category.Get(id);
            if (cate == null)
            {
                return NotFound();
            }
            else
            {
                cate.name = name;
                await _category.Update(id, cate);
                return Ok(new
                {
                    msg = "Updated!!!",
                    category = cate
                });
            }
        }

        [Authorize(Roles = "1")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _category.Delete(id);
            return Ok(new{msg = "Deleted!"});
        }
    }
}
