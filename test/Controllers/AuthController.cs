﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using test.Models;
using test.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace test.Controllers
{
    [Route("api/auth")]
    public class AuthController : Controller
    {
        private IUserRepository _user;
        private string secrectKey = "needtogetthisfromenvironment";

        public AuthController(IUserRepository user)
        {
            _user = user;
        }

        [HttpPost]
        [Route("sign-up")]
        public async Task<IActionResult> Signup(string name, string email, string address, string phoneNumber, string password)
        {

            var tmp = await _user.FindByEmail(email);
           
            if (tmp != null)
            {
                return StatusCode(500, "Email does exist! ");
            }
            var u = new User
            {
                name = name,
                email = email,
                address = address,
                phone_number = phoneNumber,
                password = password
            };
            await _user.Create(u);
            return Ok(new { msg = "Success!", user = u });
        }

        [HttpPost]
        [Route("sign-in")]
        public async Task<IActionResult> SignIn(string email, string password)
        {
            var user = await _user.FindByEmail(email);

            if (user == null || !user.password.Equals(password))
            {
                return BadRequest();
            }

            var token = genToken(user);
            return Ok(new { token = genToken(user) });
        }

        private string genToken(User u)
        {

            var key = new SymmetricSecurityKey(Encoding.Default.GetBytes(this.secrectKey));
            var claims = new Claim[]{
                new Claim(JwtRegisteredClaimNames.NameId, u.email),
                new Claim(JwtRegisteredClaimNames.Jti,u.email),

                new Claim(ClaimTypes.Role,u.role+""),
                new Claim(JwtRegisteredClaimNames.Exp, $"{new DateTimeOffset(DateTime.Now.AddDays(1)).ToUnixTimeSeconds()}"),
                new Claim(JwtRegisteredClaimNames.Nbf, $"{new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()}")
            };
            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
            );
            var tokenStr = new JwtSecurityTokenHandler().WriteToken(token);

            return tokenStr;
        }

       
    }
}