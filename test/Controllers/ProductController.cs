﻿using System;
using System.Threading.Tasks;
using test.Models;
using test.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace test.Controllers
{
    //[Authorize(Roles = "1")]
    [Route("api/product")]
    public class ProductController : Controller{
        private readonly IProductRepository _product;

        public ProductController(IProductRepository iProductRepository){
            _product = iProductRepository;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> getProductById(int id){
            var product = await _product.getProductById(id);
            return Ok(product);
        }

        [HttpGet]
        [Route("list")]
        public async Task<IActionResult> getProducts(int perpage, int page = 1)
        {
            if (perpage == 0)
            {
                var products = await _product.getAll();
                return Ok(products);
            }
            else
            {
                var products = await _product.paginate(perpage, page);
                return Ok(products);
            }
        }


        [Authorize(Roles = "1")]
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create(int id, string name,
                                               double price, int number, string content,
                                                string image, int groupid, int cateid){


            var p = new Product
            {
                name = name,
                price = price,
                number = number,
                content = content,
                image = image,
                groupid = groupid,
                cateid = cateid
            };
            await _product.Create(p);
            return Ok(new
           {
               msg = "Success!",
               product = p
           });
        }

        [Authorize(Roles = "1")]
        [HttpPut("{id}/group/{fk1}/cate/{fk2}")]
        public async Task<IActionResult> Update(int id, string name,
                                               double price, int number, string content,
                                                string image, int fk1, int fk2){

            var item = await _product.Get(id);
            if (item == null) return NotFound();
            item.name = item.name != null ? name : item.name;
            item.price = item.price != 0 ? price : item.price;
            item.number = item.number != 0 ? number : item.number;
            item.content = item.content != null ? content : item.content;
            item.image = item.image != null ? image : item.image;
            item.groupid = item.groupid != 0 ? fk1 : item.groupid;
            item.cateid = item.cateid != 0 ? fk2 : item.cateid;
             await _product.Update(id, item);
            return Ok(new
            {
                msg = "Updated!",
                question = item
            });
        }

        [Authorize(Roles = "1")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _product.Get(id);
            if (item == null) return NotFound();
            await _product.Delete(id);
            return Ok(new
            {
                msg = "Deleted!"
            });
        }
    }
}
