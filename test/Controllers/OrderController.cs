﻿using System;
using System.Threading.Tasks;
using test.Models;
using test.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;

namespace test.Controllers
{
    [Route("api/order")]
    public class OrderController : Controller
    {
        private readonly IOrderRepository _order;
        private IProductRepository _product;
        public Product product = null;
 
        public OrderController(IOrderRepository iOrderRepository, 
                               IProductRepository iProductRepository, 
                               IOptions<Product> optionsAccessor)
        {
            _order = iOrderRepository;
            _product = iProductRepository;
            product = optionsAccessor.Value;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> getOrderById(int id)
        {
            var order = await _order.getOrderById(id);
            return Ok(order);
        }

        [Authorize (Roles = "1")]
        [HttpGet("list")]
        public async Task<IActionResult> getOrders()
        {
            var orders = await _order.getOrderDetail();
            return Ok(orders);
        }

        [HttpGet("{user_id}")]
        public async Task<ActionResult> getOrderByUser(int user_id){
            var orders = await _order.getOrderByUser(user_id);
            return Ok((orders));
        }

        [HttpPost("create")]
        public async Task<ActionResult> Create(string name, string address,
                                               string phoneNumber, int number,
                                               int user_id, int product_id){
           
            product = await _product.getProductById(product_id);

            var item = new Order
            {
                name = name,
                address = address,
                number = number,
                phone_number = phoneNumber,
                calculate = number * product.price,
                userid = user_id,
                productid = product_id
            };
            await _order.Create(item);
            return Ok(new
            {
                msg = "Success!",
                order = item
            });
            
        }

        [HttpPut("{id}/user/{fk1}/product/{fk2}")]
        public async Task<IActionResult> Update(int id, string name,
                                              string address, int number, string phoneNumber,
                                              int fk1, int fk2)
        {

            var item = await _order.Get(id);
            if (item == null) return NotFound();
            item.name = item.name != null ? name : item.name;
            item.address = item.address != null ? address : item.address;
            item.number = item.number != 0 ? number : item.number;
            item.phone_number = item.phone_number != null ? phoneNumber : item.phone_number;
            item.userid = item.userid != 0 ? fk1 : item.userid;
            item.productid = item.productid != 0 ? fk2 : item.productid;
            await _order.Update(id, item);
            return Ok(new
            {
                msg = "Updated!",
                question = item
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var item = await _order.Get(id);
            if (item == null) return NotFound();
            await _order.Delete(id);
            return Ok(new
            {
                msg = "Deleted!"
            });
        }

    }

}
