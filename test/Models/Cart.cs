﻿using System;
namespace test.Models
{
    public class Cart
    {
        public int id{ 
            get; set; 
        }

        public int productid{
            get; set;
        }

        public virtual Product product{
            get; set; 
        }

        public int userid{
            get; set;
        }

        public virtual User user{
            get; set;
        }

      
    }
}
