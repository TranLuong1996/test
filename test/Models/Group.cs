﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace test.Models
{
    public class Group
    {
        public int id{
            get; set;
        }

        public int cateid { get; set; }

        public virtual Category cate
        {
            get; set;
        }

        public string name{
            get; set;
        }

    }
}
