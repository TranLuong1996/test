﻿using System;
namespace test.Models
{
    public class Order
    {
        public int id{
            get; set;
        }

        public string name{
            get; set;
        }

        public string address{
            get; set;
        }

        public string phone_number{
            get; set;
        }

        public int number { get; set; }

        public double calculate { get; set; }

        public int productid
        {
            get; set;
        }

        public virtual Product product
        {
            get; set;
        }

        public int userid
        {
            get; set;
        }

        public virtual User user{
            get; set;
        }

    }
}
