﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace test.Models
{
    public class Product
    {
        public int id
        {
            get; set;
        }

        [Column(TypeName = "TEXT")]
        [MaxLength(100)]
        public string name
        {
            get; set;
        }
        public double price
        {
            get; set;
        }

        public int number{
            get; set;
        }
        public string content{
            get; set;
        }

        public string image{
            get; set;
        }

        public int groupid { get; set; }

        public virtual Group group{
            get; set;
        }

        public int cateid { get; set; }

        public virtual Category cate
        {
            get; set;
        }
    }
}

