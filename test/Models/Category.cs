﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace test.Models
{
    public class Category
    {
        public int id{
            get; set;
        }

        [Column(TypeName = "TEXT")]
        [MaxLength(100)]
        public string name{
            get; set;
        }
    }
}
