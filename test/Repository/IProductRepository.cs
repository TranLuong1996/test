﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using test.Models;

namespace test.Repository
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        Task<Product> getProductById(int id);
        
        Task<List<Product>> getProducts();
    }
}
