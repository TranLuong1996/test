﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using test.Models;

namespace test.Repository
{
    public interface ICartRepository : IBaseRepository<Cart>
    {
        Task<List<Cart>> getCartByUser(int user_id);
    }
}
