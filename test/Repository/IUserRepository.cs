﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using test.Models;

namespace test.Repository
{
    public interface IUserRepository : IBaseRepository<User>
    {
        Task<User> FindByEmail(string email);
    }
}
