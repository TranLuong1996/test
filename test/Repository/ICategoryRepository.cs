﻿using System;
using System.Threading.Tasks;
using test.Models;

namespace test.Repository
{
    public interface ICategoryRepository : IBaseRepository<Category>
    {
        Task<Category> getCateById(int id);
    }
}
