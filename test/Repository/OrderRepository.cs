﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using test.Models;
using Microsoft.EntityFrameworkCore;

namespace test.Repository
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<List<Order>> getOrderDetail()
        {
            return await _context.order.
                                Include(o => o.user).
                                Include(o => o.product).
                                ToListAsync();
        }

        public async Task<List<Order>> getOrderByUser(int userid)
        {
            return await _context.order.
                                 Include(o => o.product).
                                 Where(o => o.userid == userid).
                                 ToListAsync();
                             
        }

        public async Task<Order> getOrderById(int id)
        {
            return await _context.order.Include(o => o.user).
                                 Include(o => o.product).SingleAsync(o => o.id == id);
        }
    }
}
