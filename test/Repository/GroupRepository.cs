﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using test.Models;
using Microsoft.EntityFrameworkCore;

namespace test.Repository
{
    public class GroupRepository : Repository<Group>, IGroupRepository
    {
        public GroupRepository(ApplicationDbContext context) : base(context)
        {
        }


        public async Task<Group> getGroupById(int id)
        {
            return await _context.group.Include(g => g.cate).SingleAsync(g => g.id == id);
        }
    }
}
