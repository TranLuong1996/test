﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using test.Models;

namespace test.Repository
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        Task<List<Order>> getOrderDetail();

        Task<List<Order>> getOrderByUser(int userid);

        Task<Order> getOrderById(int id);
    }
}
