﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using test.Models;

namespace test.Repository
{
    public class CartRepository : Repository<Cart>, ICartRepository
    {
        public CartRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<List<Cart>> getCartByUser(int user_id)
        {
            return await _context.cart.
                                 Include(o => o.product).
                                 Where(o => o.userid == user_id).
                                 ToListAsync();
        }
    }
}
