﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using test.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace test.Repository
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<Product> getProductById(int id)
        {
            return await _context.product.Include(p => p.group).
                                 Include(p => p.cate).SingleAsync(p => p.id == id);

        }

        public async Task<List<Product>> getProducts()
        {
            return await _context.product.
                                 Include(p => p.group).
                                 Include(p => p.cate).
                                 ToListAsync();
        }
    }
}
