﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using test.Models;

namespace test.Repository
{
    public interface IGroupRepository : IBaseRepository<Group>
    {
        Task<Group> getGroupById(int id);
    }
}
